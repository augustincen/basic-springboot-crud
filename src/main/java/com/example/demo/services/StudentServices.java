package com.example.demo.services;

import com.example.demo.repository.StudentRepository;
import com.example.demo.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServices {
    @Autowired
    private StudentRepository studentRepository;
    public List<Student> getStudentServices(){
       return studentRepository.findAll();

    }

    public void addNewStudent(Student student){
        Optional<Student> studentOptional = studentRepository.findStudentByEmail(student.getEmail());
        System.out.println(studentOptional);
        if(studentOptional.isPresent()){
            throw new IllegalStateException("Email Taken");
        }
        studentRepository.save(student);
    }

    public void deleteStudent(Long id){
    boolean exist = studentRepository.existsById(id);
    if(!exist){
        throw new IllegalStateException("Student with id" + id + "does not exist");
    }
    studentRepository.deleteById(id);
    }

    @Transactional
    public void updateStudent(Long id, String name, String email){
        boolean exist = studentRepository.existsById(id);
        Student newStudent = studentRepository.findStudentById(id);
        System.out.println(newStudent);
        if(!exist){
            throw new IllegalStateException("Student with id" + id + "does not exist");
        }
        newStudent.setName(name);
        newStudent.setEmail(email);
        //newStudent.setDob(student.getDob());
        studentRepository.save(newStudent);
    }
}
