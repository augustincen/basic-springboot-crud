package com.example.demo.config;

import com.example.demo.repository.StudentRepository;
import com.example.demo.student.Student;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository){
        return args -> {
            Student marry = new Student(
                    "Marry",
                    "Marry@gmail.com",
                    LocalDate.of(2000, Month.DECEMBER,20)

            );

            Student Alex = new Student(
                    "Alex",
                    "Alex@gmail.com",
                   LocalDate.of(1999,Month.JANUARY,18)
            );

            studentRepository.saveAll(List.of(marry,Alex));
        };
    }
}
