package com.example.demo.repository;

import com.example.demo.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository


public interface StudentRepository extends JpaRepository<Student,Long> {

   public List<Student> findAll();

   Optional<Student> findStudentByEmail(String email);

   Student findStudentById(Long id);


}
