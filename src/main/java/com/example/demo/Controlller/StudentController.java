package com.example.demo.Controlller;

import com.example.demo.services.StudentServices;
import com.example.demo.student.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@RestController
@RequestMapping("api/v1/student")
public class StudentController {

        @Autowired
   private StudentServices studentServices;

    @GetMapping()
    public List<Student> getStudent(){
        return studentServices.getStudentServices();
    }

    @PostMapping()
    public void registerNewStudent(@RequestBody Student student){
        studentServices.addNewStudent(student);
    }

    @DeleteMapping(path = "{studentId}")
    public void deleteStudent(@PathVariable("studentId") Long id){
        studentServices.deleteStudent(id);
    }
    @PutMapping(path = "{studentId}")
    public void editStudent(@PathVariable("studentId") Long id, @RequestParam(required = false) String name, @RequestParam(required = false) String email){
        studentServices.updateStudent(id,name,email);
    }
}
